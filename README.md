# Penguin Bot v2

Elegoo Penguin Bot v2 source, with slight modifications to get the project to compile with Platform IO IDE extension.

## Links
- [Official Source](https://github.com/elegooofficial/ELEGOO-Penguin-Bot-V2.0) - Project files packaged as Zip file on GitHub
- [Product Page](https://www.elegoo.com/products/elegoo-penguin-bot)